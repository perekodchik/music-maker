import { createTheme } from "@mui/material";
import { indigo } from "@mui/material/colors";

export const darkTheme = createTheme({
	palette: {
		mode: "dark",
		primary: indigo,
	},
});
