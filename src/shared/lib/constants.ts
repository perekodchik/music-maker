import { Note, Instruments } from "./types";

export const octave: Note[] = [
	{ name: "C" },
	{ name: "C", modificator: "sharp" },
	{ name: "D" },
	{ name: "D", modificator: "sharp" },
	{ name: "E" },
	{ name: "F" },
	{ name: "F", modificator: "sharp" },
	{ name: "G" },
	{ name: "G", modificator: "sharp" },
	{ name: "A" },
	{ name: "A", modificator: "sharp" },
	{ name: "B" },
];

export const instrumentsToPaths: Record<Instruments, string> = {
	piano: "/sounds/piano/piano-C4.mp3",
};
