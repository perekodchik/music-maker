type NoteNames = "C" | "D" | "E" | "F" | "G" | "A" | "B";

type NoteModificator = "sharp" | "flat";

export type Note = {
	name: NoteNames;
	modificator?: NoteModificator;
};

export type Instruments = "piano";
