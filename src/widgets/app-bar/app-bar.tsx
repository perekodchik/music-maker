import { useThemeSwitch } from "@features/theme-switcher";
import {
	AppBar as MuiAppBar,
	Button,
	IconButton,
	Toolbar,
	Typography,
} from "@mui/material";

export const AppBar = () => {
	const { toggleTheme } = useThemeSwitch();

	return (
		<MuiAppBar position="static">
			<Toolbar>
				<IconButton
					size="large"
					edge="start"
					color="inherit"
					aria-label="menu"
					sx={{ mr: 2 }}
				>
					<div style={{ transform: "rotate(90deg)" }}>|||</div>
				</IconButton>
				<Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
					MusicMaker
				</Typography>
				<Button color="inherit" onClick={() => toggleTheme()}>
					Change Theme
				</Button>
			</Toolbar>
		</MuiAppBar>
	);
};
