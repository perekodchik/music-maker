import React from "react";
import { Box, styled } from "@mui/material";

import { octave } from "@shared/lib/constants";

import { PianoKey } from "../../molecules";

const KeyboardWrapper = styled(Box)`
	display: flex;
	flex-direction: row;
`;

export const PianoKeyboard = () => {
	return (
		<KeyboardWrapper style={{ display: "flex", flexDirection: "row" }}>
			{octave.map((note, index) => (
				<PianoKey note={note} key={note.name + index} pitch={index} />
			))}
		</KeyboardWrapper>
	);
};
