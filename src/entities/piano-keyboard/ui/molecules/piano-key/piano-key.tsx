import React, { useCallback } from "react";

import { Note } from "@shared/lib/types";
import { Box, styled } from "@mui/material";
import { BlackKey, WhiteKey } from "../../atoms";
import useSound from "use-sound";
import { instrumentsToPaths } from "@shared/lib/constants";

type Props = {
	note: Note;
	pitch: number;
};

const BlackKeyWrapper = styled(BlackKey)`
	position: absolute;
	z-index: 1;
	top: 0;
	right: -${({ theme }) => theme.spacing(2)};
`;

const BlackKeyBox = styled(Box)`
	position: relative;
	width: 0;
`;

export const PianoKey = ({ note, pitch }: Props) => {
	const [play] = useSound(instrumentsToPaths["piano"], {
		playbackRate: Math.pow(2, pitch / 12),
	});

	const handleClick = useCallback(() => {
		play();
	}, [play]);

	return note.modificator ? (
		<BlackKeyBox>
			<BlackKeyWrapper onClick={handleClick} label={note.name + "#"} />
		</BlackKeyBox>
	) : (
		<WhiteKey onClick={handleClick} label={note.name} />
	);
};
