import React from "react";

import { Box, BoxProps, styled, Typography, useTheme } from "@mui/material";

const Wrapper = styled(Box)`
	width: ${({ theme }) => theme.spacing(6)};
	height: ${({ theme }) => theme.spacing(20)};
	display: flex;
	align-items: flex-end;
	justify-content: center;
	background-color: ${({ theme }) => theme.palette.common.white};
	border: ${({ theme }) => theme.spacing(0.5)} solid
		${({ theme }) => theme.palette.common.black};
	border-bottom-left-radius: ${({ theme }) => theme.spacing(2)};
	border-bottom-right-radius: ${({ theme }) => theme.spacing(2)};
	&:hover {
		cursor: pointer;
	}
`;

type Props = BoxProps & {
	label?: string;
};

export const WhiteKey = ({ label, ...props }: Props) => {
	const theme = useTheme();

	return (
		<Wrapper {...props}>
			<Typography color={theme.palette.common.black}>{label}</Typography>
		</Wrapper>
	);
};
