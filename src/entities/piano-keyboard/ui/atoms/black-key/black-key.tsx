import React from "react";
import { Box, styled, Typography, BoxProps, useTheme } from "@mui/material";

const Wrapper = styled(Box)`
	width: ${({ theme }) => theme.spacing(4)};
	height: ${({ theme }) => theme.spacing(15)};
	display: flex;
	align-items: flex-end;
	justify-content: center;
	background-color: ${({ theme }) => theme.palette.common.black};
	border-bottom-left-radius: ${({ theme }) => theme.spacing(1)};
	border-bottom-right-radius: ${({ theme }) => theme.spacing(1)};
	&:hover {
		cursor: pointer;
	}
`;

type Props = BoxProps & {
	label?: string;
};

export const BlackKey = ({ label, ...props }: Props) => {
	const theme = useTheme();

	return (
		<Wrapper {...props}>
			<Typography unselectable="on" color={theme.palette.common.white}>
				{label}
			</Typography>
		</Wrapper>
	);
};
