import React from "react";
import { ThemeProvider, CssBaseline } from "@mui/material";
import { lightTheme, darkTheme } from "@shared/ui/theme";
import { AppBar } from "@widgets/app-bar";
import { MainConnector } from "@pages";
import { useThemeSwitch } from "@features/theme-switcher";

export function App() {
	const { theme } = useThemeSwitch();

	return (
		<ThemeProvider theme={theme === "light" ? lightTheme : darkTheme}>
			<CssBaseline enableColorScheme />
			<AppBar />
			<MainConnector />
		</ThemeProvider>
	);
}
