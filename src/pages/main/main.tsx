import React from "react";
import { Container, styled } from "@mui/material";

import { PianoKeyboard } from "@entities/piano-keyboard";

const ContainerStyled = styled(Container)`
	display: flex;
	flex-direction: column;
	align-items: center;
`;

export const Main = () => {
	return (
		<ContainerStyled>
			<PianoKeyboard />
		</ContainerStyled>
	);
};
