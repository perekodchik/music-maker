import { createStore, createApi } from "effector";
import { useStore } from "effector-react";

type SelectedTheme = "light" | "dark";

const $theme = createStore<SelectedTheme>("light");

const { toggleTheme } = createApi($theme, {
	toggleTheme: (theme) => (theme === "light" ? "dark" : "light"),
});

export const useThemeSwitch = () => {
	const theme = useStore($theme);
	return { theme, toggleTheme };
};
